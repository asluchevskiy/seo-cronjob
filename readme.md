## Установка и настройка

1. установить зависимости из requiments.txt
2. иметь установленный локально MongoDB сервер
3. настроить доступы в production_settings.py
4. запустить

### опции запуска

```
$ python main.py --help
Usage: main.py [options]

Options:
  -h, --help         show this help message and exit
  -j JOB, --job=JOB  job name: indexation, positions
  -n, --new          create new job task
  -o, --once         run once
```

* без опции `new` скрипт будет запущен только в режиме слежения за уже созданным заданием
* с опцией `once` скрипт отработает 1 раз и не будет проверять статус задач каждую минуту