# -*- coding: utf-8 -*-
from jobs.indexation import IndexationJob
from jobs.positions import PositionsJob, AllPositionsJob, WithoutTicketsPositionsJob
from jobs.listings import ListingsJob
from optparse import OptionParser
import logging
import string
import time

JOBS = {
    'indexation': IndexationJob,
    'positions': PositionsJob,
    'all_positions': AllPositionsJob,
    'without_tickets_positions': WithoutTicketsPositionsJob,
    'listings': ListingsJob,
}

if __name__ == '__main__':
    logger = logging.getLogger('requests.packages.urllib3.connectionpool')
    # logger.setLevel(level=logging.ERROR)
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s\t%(levelname)s\t%(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    parser = OptionParser()
    parser.add_option('-j', '--job', dest='job', help='job name: %s' % string.join(JOBS.keys(), ', '))
    parser.add_option('-n', '--new', action='store_true', dest='new', default=False, help='create new job task')
    parser.add_option('-o', '--once', action='store_true', dest='once', default=False, help='run once')
    (options, args) = parser.parse_args()

    if not options.job or options.job not in JOBS:
        parser.print_help()
        exit()

    j = JOBS[options.job]()
    j.run(new_job=options.new)

    if not options.once:
        while not j.is_job_finished():
            time.sleep(30)
            try:
                j.run(new_job=False)
            except Exception as ex:
                logging.error(ex)
