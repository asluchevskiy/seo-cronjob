# -*- coding: utf-8 -*-
import datetime
import logging
import pymongo
from peewee import *
from playhouse.shortcuts import model_to_dict
from settings import PSQL_DATABASE, PSQL_HOST, PSQL_PORT, PSQL_PASSWORD, PSQL_USER, MONGO_DB

db = PostgresqlDatabase(PSQL_DATABASE, user=PSQL_USER, password=PSQL_PASSWORD, host=PSQL_HOST, port=PSQL_PORT)
mongo_db = pymongo.MongoClient()[MONGO_DB]


class SeoEventsBaseModel(Model):
    event_id = PrimaryKeyField()
    alias = CharField()
    title = CharField()
    region_id = IntegerField()
    region_nom_case = CharField()
    reg_prep_case = CharField()
    event_type = CharField()
    event_type_accus = CharField()
    url = CharField(db_column='production_url')


class SeoEventsWithTickets(SeoEventsBaseModel):
    class Meta:
        database = db
        schema = 'public'
        db_table = 'seo_parsing_events_w_tickets'


class SeoEventsWithoutTickets(SeoEventsBaseModel):
    class Meta:
        database = db
        schema = 'public'
        db_table = 'seo_parsing_events_wo_tickets'


class CheckIndexationResult(Model):
    event_id = IntegerField()
    alias = CharField(max_length=255)
    title = CharField(max_length=255)
    region_id = IntegerField()
    region_nom_case = CharField(max_length=255)
    has_tickets = BooleanField()
    url = CharField(max_length=255)
    is_indexed_google = BooleanField()
    is_indexed_yandex = BooleanField()
    timestamp = DateTimeField()

    class Meta:
        database = db
        db_table = 'check_indexation_result'


class Mask(Model):
    mask = CharField()

    class Meta:
        database = db
        db_table = 'check_position_mask'


class Website(Model):
    website = CharField()
    with_subdomains = BooleanField(default=False)
    website_type = IntegerField()  # 1 - ponominalu.ru, 2 - competitors

    class Meta:
        database = db
        db_table = 'check_position_website'


class CheckPositionResult(Model):
    event_id = IntegerField()
    website = ForeignKeyField(Website)
    mask = ForeignKeyField(Mask)
    alias = CharField(max_length=255)
    title = CharField(max_length=255)
    region_id = IntegerField()
    region_nom_case = CharField(max_length=255)
    reg_prep_case = CharField()
    query = CharField()
    search_engine = CharField(max_length=6, choices=('google', 'yandex'))
    response_url = TextField()
    response_title = CharField()
    response_text = TextField(null=True)
    position_number = IntegerField()
    has_tickets = BooleanField()
    timestamp = DateTimeField()

    class Meta:
        database = db
        db_table = 'check_position_result'


class CheckPositionAd(Model):
    event_id = IntegerField()
    query = CharField()
    mask = ForeignKeyField(Mask)
    region_id = IntegerField()
    region_nom_case = CharField(max_length=255)
    position_number = IntegerField()
    title = CharField(max_length=255)
    owner = CharField(max_length=255)
    url = TextField(null=True)
    website = CharField(null=True)
    contact_info = TextField(null=True)
    timestamp = DateTimeField()

    class Meta:
        database = db
        db_table = 'check_position_yandex_ad'


class Region(Model):
    title = CharField()
    proxy_region = CharField(null=True)
    yandex_region_id = IntegerField()

    class Meta:
        database = db
        db_table = 'seo_region'


class ListingsModel(Model):
    region_id = IntegerField()
    region_nom_case = CharField()
    publish_f_id = IntegerField()
    url = CharField(db_column='production_url', primary_key=True)
    class Meta:
        database = db


class CheckIndexationListingsResult(Model):
    region_id = IntegerField()
    region_nom_case = CharField(max_length=255)
    publish_f_id = IntegerField()
    url = CharField(max_length=255)
    listing_type = CharField(index=True, null=True)
    is_indexed_google = BooleanField()
    is_indexed_yandex = BooleanField()
    timestamp = DateTimeField()

    class Meta:
        database = db
        db_table = 'check_indexation_listings_result'



class AfishaModel(ListingsModel):
    listing_type = 'afisha'

    class Meta:
        db_table = 'afisha_urls'


class CategoryModel(ListingsModel):
    listing_type = 'category'

    class Meta:
        db_table = 'category_urls'


class GrouptagModel(ListingsModel):
    listing_type = 'grouptag'

    class Meta:
        db_table = 'grouptag_urls'


class TagModel(ListingsModel):
    listing_type = 'tag'

    class Meta:
        db_table = 'tag_urls'


class VenueModel(ListingsModel):
    listing_type = 'venue'

    class Meta:
        db_table = 'venue_urls'


# if __name__ == '__main__':
    # CheckIndexationListingsResult.create_table()
    # for model in (CategoryModel, GrouptagModel, TagModel, VenueModel, AfishaModel):
    #     print model.listing_type, model.select().count()
