# -*- coding: utf-8 -*-
from urlparse import urlparse
from datetime import datetime
from playhouse.shortcuts import model_to_dict
import pymongo
import logging
import requests
from . import Job
from models import SeoEventsWithTickets, SeoEventsWithoutTickets, \
    CheckPositionResult, Mask, Website, Region, CheckPositionAd, db as pg
import settings


class PositionsJob(Job):
    job_name = 'positions'
    methods = ('yandex_search', 'google_search')
    models = (SeoEventsWithTickets, )

    def __init__(self):
        super(PositionsJob, self).__init__()
        # self.website_list = {w.website: w.id for w in Website.select()}
        self.website_list = Website.select()
        self.regions = {r.title: r for r in Region.select()}
        self.collection.ensure_index('event_id')
        self.collection.ensure_index('query')

    def copy_data(self):
        self.mask_list = Mask.select()
        self.collection.remove({})
        for model in self.models:
            for item in model.select():
                for mask in self.mask_list:
                    _item = model_to_dict(item)
                    _item['_meta'] = {'processed': False, 'exported': False}
                    _item['has_tickets'] = True if isinstance(item, SeoEventsWithTickets) else False
                    query = mask.mask % _item
                    _item['mask_id'] = mask.id
                    _item['query'] = query
                    self.collection.save(_item)

    def run_next_task(self):
        items = [item for item in self.collection.find({'_meta.processed': False}).limit(settings.BATCH_COUNT)]
        if not items:
            self._mark_job_finished()  # mark job is done
            return

        _items = {}  # group by regions
        for item in items:
            key = '_other' if item['region_nom_case'] not in self.regions else item['region_nom_case']
            if key not in _items:
                _items[key] = []
            _items[key].append(item)

        for method in self.methods:
            logging.info('running new task %s with %s items' % (method, len(items)))
            url = '%s/task/%s/?api_key=%s' % (settings.API_URL, method, settings.API_KEY)

            def process(self, json_data, items):
                res = requests.post(url=url, json=json_data)
                if res.status_code != 200:
                    logging.error(res.json())
                    return
                task_id = res.json()['id']
                self._add_task({'id': res.json()['id'], 'method': method})
                self.collection.update({'_id': {'$in': [item['_id'] for item in items]}},
                                       {'$addToSet': {'task_id': task_id}}, multi=True)

            if method == 'yandex_search':
                for region in _items:
                    data = {
                        'query_list': [item['query'] for item in _items[region]],
                        'total_results': 100,
                        'with_ad': False,
                        'resolve_ad_urls': True if self.job_name == 'positions' else False,
                        'noreask': True,
                        'yandex_region': 213 if region == '_other' else self.regions[region].yandex_region_id,
                        'emails': settings.EMAILS_LIST_API_TASK
                    }
                    process(self, data, _items[region])
            if method == 'google_search':
                data = {
                    'query_list': [item['query'] for item in items],
                    'total_results': 100,
                    'emails': settings.EMAILS_LIST_API_TASK
                }
                process(self, data, items)

    def _process_item_result(self, item, method):
        query = item['options']['query']
        if method == 'yandex_search':
            key = '_result.yandex'
        else:
            key = '_result.google'
        result = []
        found_websites = set()
        task_id = item['task_id']
        for res in item['result']['data']:
            domain = urlparse(res['url']).netloc.lower()
            for ws in self.website_list:
                if (ws.with_subdomains and domain.endswith('.' + ws.website)) or domain == ws.website:
                    if ws.website in found_websites:
                        continue
                    result.append({
                        'url': res['url'],
                        'title': res['title'],
                        'text': res['text'],
                        'website': ws.website,
                        'website_id': ws.id,
                        'position': res['position']
                    })
                    found_websites.add(ws.website)
        self.collection.update({'query': query, 'task_id': task_id}, {'$set': {key: result}}, multi=True)
        if method == 'yandex_search' and item['result']['ad']:
            ad_result = []
            for ad in item['result']['ad']:
                ad['website'] = urlparse(ad['resolved_url']).netloc.lower() if 'resolved_url' in ad else None
                ad_result.append(ad)
            self.collection.update({'query': query, 'task_id': task_id},
                                   {'$set': {'_result.yandex_ad': ad_result}}, multi=True)

    def mark_as_processed(self):
        res = self.collection.update({'_result.yandex': {'$exists': True},
                                      '_result.google': {'$exists': True},
                                      '_meta.processed': False},
                                     {'$set': {'_meta.processed': True}}, multi=True)
        self._update_job_counters(inc=res['n'])

    def export(self):
        with pg.atomic():
            data = []
            ad_data = []
            for item in self.collection.find({'_meta.processed': True, '_meta.exported': False}).sort(
                    [('event_id', pymongo.ASCENDING), ('mask_id', pymongo.ASCENDING)]):
                for search_engine in ('yandex', 'google'):
                    for res in item['_result'][search_engine]:
                        _item = {
                            'event_id': item['event_id'],
                            'website': res['website_id'],
                            'mask': item['mask_id'],
                            'alias': item['alias'],
                            'title': item['title'],
                            'region_id': item['region_id'],
                            'region_nom_case': item['region_nom_case'],
                            'reg_prep_case': item['reg_prep_case'],
                            'query': item['query'],
                            'search_engine': search_engine,
                            'response_url': res['url'],
                            'response_title': res['title'],
                            'response_text': res['text'],
                            'position_number': res['position'],
                            'has_tickets': item['has_tickets'],
                            'timestamp': datetime.now()
                        }
                        data.append(_item)
                if item['mask_id'] == 1 and self.job_name == 'positions':
                    for res in item['_result'].get('yandex_ad', []):
                        _item = {
                            'event_id': item['event_id'],
                            'query': item['query'],
                            'mask': item['mask_id'],
                            'region_id': item['region_id'],
                            'region_nom_case': item['region_nom_case'],
                            'position_number': res['position'],
                            'title': res['title'],
                            'owner': res['path'],
                            'url': res.get('resolved_url'),
                            'website': res.get('website'),
                            'contact_info': res.get('contact_info'),
                            'timestamp': datetime.now()
                        }
                        ad_data.append(_item)
            if data:
                CheckPositionResult.insert_many(data).execute()
                if ad_data:
                    CheckPositionAd.insert_many(ad_data).execute()
                self.collection.update({'_meta.processed': True, '_meta.exported': False},
                                       {'$set': {'_meta.exported': True}}, multi=True)


class AllPositionsJob(PositionsJob):
    job_name = 'all_positions'
    models = (SeoEventsWithoutTickets, SeoEventsWithTickets)


class WithoutTicketsPositionsJob(PositionsJob):
    job_name = 'without_tickets_positions'
    models = (SeoEventsWithoutTickets, )
