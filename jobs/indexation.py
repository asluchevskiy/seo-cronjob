# -*- coding: utf-8 -*-
from . import Job
from models import SeoEventsWithoutTickets, SeoEventsWithTickets, CheckIndexationResult, db as pg
from playhouse.shortcuts import model_to_dict, dict_to_model
from datetime import datetime
import requests
import settings
import logging


class IndexationJob(Job):
    job_name = 'indexation'
    methods = ('yandex_check_url', 'google_check_url')

    def __init__(self):
        super(IndexationJob, self).__init__()
        self.collection.ensure_index('url')

    def copy_data(self):
        self.collection.remove({})
        for model in (SeoEventsWithTickets, SeoEventsWithoutTickets):
            for item in model.select():
                _item = model_to_dict(item)
                _item['_meta'] = {'processed': False, 'exported': False}
                _item['has_tickets'] = True if isinstance(item, SeoEventsWithTickets) else False
                self.collection.save(_item)

    def _get_task_post_data(self, items, method=None):
        return [{
            'url_list': [item['url'] for item in items],
            'emails': settings.EMAILS_LIST_API_TASK
        }]

    def _process_item_result(self, item, method):
        url = item['options']['url']
        result = item['result']
        if method == 'yandex_check_url':
            key = '_result.is_indexed_yandex'
        else:
            key = '_result.is_indexed_google'
        self.collection.update({'url': url}, {'$set': {key: result}}, multi=True)

    def mark_as_processed(self):
        res = self.collection.update({'_result.is_indexed_yandex': {'$exists': True},
                                      '_result.is_indexed_google': {'$exists': True},
                                      '_meta.processed': False},
                                     {'$set': {'_meta.processed': True}}, multi=True)
        self._update_job_counters(inc=res['n'])

    def export(self):
        with pg.atomic():
            data = []
            for item in self.collection.find({'_meta.processed': True, '_meta.exported': False}):
                _item = {
                    'event_id': item['event_id'],
                    'alias': item['alias'],
                    'title': item['title'],
                    'region_id': item['region_id'],
                    'region_nom_case': item['region_nom_case'],
                    'url': item['url'],
                    'has_tickets': item['has_tickets'],
                    'is_indexed_google': item['_result'].get('is_indexed_google'),
                    'is_indexed_yandex': item['_result'].get('is_indexed_yandex'),
                    'timestamp': datetime.now()
                }
                data.append(_item)
            if data:
                CheckIndexationResult.insert_many(data).execute()
                self.collection.update({'_meta.processed': True, '_meta.exported': False},
                                       {'$set': {'_meta.exported': True}}, multi=True)
