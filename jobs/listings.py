# -*- coding: utf-8 -*-
from jobs.indexation import IndexationJob
from models import CategoryModel, GrouptagModel, TagModel, VenueModel, AfishaModel,\
    CheckIndexationListingsResult, db as pg
from playhouse.shortcuts import model_to_dict, dict_to_model
from datetime import datetime
import requests
import settings
import logging


class ListingsJob(IndexationJob):
    job_name = 'listings'

    def copy_data(self):
        self.collection.remove({})
        for model in (CategoryModel, GrouptagModel, TagModel, VenueModel, AfishaModel):
            for item in model.select():
                _item = model_to_dict(item)
                _item['_meta'] = {'processed': False, 'exported': False}
                _item['listing_type'] = model.listing_type
                self.collection.save(_item)

    def export(self):
        with pg.atomic():
            data = []
            for item in self.collection.find({'_meta.processed': True, '_meta.exported': False}):
                _item = {
                    'is_indexed_google': item['_result'].get('is_indexed_google'),
                    'is_indexed_yandex': item['_result'].get('is_indexed_yandex'),
                    'timestamp': datetime.now()
                }
                for key in ('region_id', 'region_nom_case', 'publish_f_id', 'url', 'listing_type'):
                    _item[key] = item[key]
                data.append(_item)
            if data:
                CheckIndexationListingsResult.insert_many(data).execute()
                self.collection.update({'_meta.processed': True, '_meta.exported': False},
                                       {'$set': {'_meta.exported': True}}, multi=True)

# if __name__ == '__main__':
#     logging.basicConfig(level=logging.INFO)
#     ListingsJob().run(new_job=True)
