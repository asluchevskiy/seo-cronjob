# -*- coding: utf-8 -*-
import requests
requests.adapters.DEFAULT_RETRIES = 3
import settings
import logging
from datetime import datetime
from models import mongo_db as db


class Job(object):
    job_name = ''
    methods = ()

    def __init__(self):
        self.db = db
        self.job = None
        self.collection = self.db[self.job_name]
        self.collection.ensure_index('_meta.processed')
        self.collection.ensure_index('_meta.exported')

    def run(self, new_job=True):
        logging.info('running with new job creation mode = %s' % new_job)
        if not self.job:
            self.job = self.db.task.find_one({'job_name': self.job_name, 'status': 'STARTED'})
        if not self.job and new_job:  # make new job if not exists
            self.job = self._get_new_job()
            logging.info('creating new job id=%s' % self.job['_id'])
            try:
                self.copy_data()  # copy data to the local mongoDB
                self._update_job_counters()  # update number of records in the job instance
            except Exception:
                self.db.task.remove({'_id': self.job['_id']})
                raise
            logging.info('total %s items' % self.job['counters']['total'])
        if self.job:
            for task in tuple(self.job['tasks']):  # check existing tasks is ready
                if self.check_task_is_ready(task):
                    logging.info('task id=%s is ready' % task['id'])
                    # if ready, save the data to local mongoDB:
                    self.save_task_data(task)
            self.mark_as_processed()  # check and mark mongoDB records as processed
            if not self.job['tasks']:  # if no tasks or all previous tasks ready
                self.run_next_task()  # run new task(s)
            self.export()  # export processed records to the external DB

            if self.is_job_finished():  # check job is done
                self.send_email()  # send a confirmation email
                logging.info('cronjob completed')
                # done ;-D

    def is_job_finished(self):
        if self.job:
            return self.job['status'] == 'FINISHED'

    def _get_new_job(self):
        job = {'job_name': self.job_name,
               'timestamp': datetime.now(),
               'tasks': [],
               'status': 'STARTED'}
        job['_id'] = self.db.task.save(job)
        return job

    def _mark_job_finished(self):
        self.job['status'] = 'FINISHED'
        self.db.task.save(self.job)

    def _update_job_counters(self, inc=0):
        if 'counters' not in self.job:
            processed = 0
        else:
            processed = self.job['counters']['processed'] + inc
        counters = {
            'processed': processed,
            'total': self.collection.count()
        }
        self.job['counters'] = counters
        self.db.task.save(self.job)

    def _add_task(self, task):
        self.job['tasks'].append(task)
        self.db.task.update({'_id': self.job['_id']}, {'$addToSet': {'tasks': task}})

    def _remove_task(self, task):
        if task in self.job['tasks']:
            self.job['tasks'].remove(task)
        self.db.task.update({'_id': self.job['_id']}, {'$pull': {'tasks': task}})

    def run_next_task(self):
        items = [item for item in self.collection.find({'_meta.processed': False}).limit(settings.BATCH_COUNT)]
        if not items:
            self._mark_job_finished()  # mark job is done
            return
        for method in self.methods:
            logging.info('running new task %s with %s items' % (method, len(items)))
            url = '%s/task/%s/?api_key=%s' % (settings.API_URL, method, settings.API_KEY)
            for data in self._get_task_post_data(items, method=method):
                # data = self._get_task_post_data(items)
                res = requests.post(url=url, json=data)
                if res.status_code != 200:
                    logging.error(res.json())
                    return
                self._add_task({'id': res.json()['id'], 'method': method})

    def check_task_is_ready(self, task):
        logging.info('checking %s task id=%s' % (task['method'], task['id']))
        url = '%s/task/%s/%s?api_key=%s' % (settings.API_URL, task['method'], task['id'], settings.API_KEY)
        logging.info(url)
        res = requests.get(url=url)
        if res.status_code != 200:
            logging.error(res.json())
            return
        return res.json()['status'] == 'FINISHED'

    def save_task_data(self, task):
        url = '%s/item/%s/?task_id=%s&api_key=%s&per_page=%s' % (settings.API_URL, task['method'],
                                                                 task['id'], settings.API_KEY, settings.BATCH_COUNT)
        logging.info(url)
        res = requests.get(url=url)
        data = res.json()['data']
        count = 0
        for item in data:
            if not item['error']:
                self._process_item_result(item, task['method'])
                count += 1
        logging.info('processed %s items for task id=%s' % (count, task['id']))
        self._remove_task(task)

    def send_email(self):
        text = 'Cronjob %(job_name)s was completed. Processed %(processed)d/%(total)d items.\n\n' \
               'This was an automated message' % dict(job_name=self.job_name,
                                                      processed=self.job['counters']['processed'],
                                                      total=self.job['counters']['total'])
        res = requests.post(
            url='%s/messages' % settings.MAILGUN_API_BASE_URL,
            auth=('api', settings.MAILGUN_API_KEY),
            data={
                'from': 'noreply@margico.com',
                'to': settings.EMAILS_LIST,
                'subject': 'cronjob %s is ready' % self.job_name,
                'text': text
            }
        )
        if res.status_code != 200:
            logging.error(res.json())
        else:
            logging.info('the email was sent')

    # need to inherit this methods:

    def _get_task_post_data(self, items):
        raise NotImplementedError()

    def _process_item_result(self, item, method):
        raise NotImplementedError()

    def copy_data(self):
        raise NotImplementedError()

    def mark_as_processed(self):
        raise NotImplementedError()

    def export(self):
        raise NotImplementedError()
