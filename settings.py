# -*- coding: utf-8 -*-
PSQL_USER = 'seo'
PSQL_PORT = 5432
PSQL_DATABASE = 'seo'
PSQL_HOST = 'localhost'
PSQL_PASSWORD = 'password'

API_URL = 'http://seo.margico.com/api/v1'
API_KEY = 'api_key'

EMAILS_LIST_API_TASK = []
EMAILS_LIST = []
BATCH_COUNT = 1000

MONGO_DB = 'seo-cronjob'

MAILGUN_API_BASE_URL = 'https://api.mailgun.net/v3/seo.margico.com'
MAILGUN_API_KEY = ''

try:
    from production_settings import *
except ImportError:
    pass
